var obj = JSON.parse($response.body);
obj={
  "receipt": {
    "receipt_type": "Production",
    "adam_id": 1530173879,
    "app_item_id": 1530173879,
    "bundle_id": "com.nhatnguyen.drumtify",
    "application_version": "3",
    "download_id": 501870461523543323,
    "version_external_identifier": 852115016,
    "receipt_creation_date": "2022-11-11 01:59:45 Etc/GMT",
    "receipt_creation_date_ms": "1668131985000",
    "receipt_creation_date_pst": "2022-11-10 17:59:45 America/Los_Angeles",
    "request_date": "2022-11-11 01:59:55 Etc/GMT",
    "request_date_ms": "1668131995197",
    "request_date_pst": "2022-11-10 17:59:55 America/Los_Angeles",
    "original_purchase_date": "2022-10-27 21:07:41 Etc/GMT",
    "original_purchase_date_ms": "1666904861000",
    "original_purchase_date_pst": "2022-10-27 14:07:41 America/Los_Angeles",
    "original_application_version": "3",
    "in_app": [{
      "quantity": "1",
      "product_id": "com.nhatnguyen.drumtify.removeads",
      "transaction_id": "540001070870487",
      "original_transaction_id": "540001070870487",
      "purchase_date": "2022-11-11 01:59:45 Etc/GMT",
      "purchase_date_ms": "1668131985000",
      "purchase_date_pst": "2022-11-10 17:59:45 America/Los_Angeles",
      "original_purchase_date": "2022-11-11 01:59:45 Etc/GMT",
      "original_purchase_date_ms": "1668131985000",
      "original_purchase_date_pst": "2022-11-10 17:59:45 America/Los_Angeles",
      "is_trial_period": "false",
      "in_app_ownership_type": "PURCHASED"
    }]
  },
  "environment": "Production",
  "status": 0
}

$done({body: JSON.stringify(obj)});